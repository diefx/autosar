/**
  * @file    Mcu.h
  * @author  Modular Ninja Team
  * @brief   Specification of MCU Driver
  * https://www.autosar.org/fileadmin/user_upload/standards/classic/20-11/AUTOSAR_SWS_StandardTypes.pdf
*/
#ifndef MCU_H
#define MCU_H

/** 
  * @brief  A pointer to such a structure is provided to the MCU initialization routines for 
  *         configuration.  
  *         [SWS_Mcu_00131]:The structure Mcu_ConfigTypeis an external data structure (i.e. implementation 
  *         specific) and shall contain the initialization data for the MCU module. It shall contain:
  *           -MCU dependent properties
  *           -Reset Configuration
  *           -Definition of MCU modes
  *           -Definition of Clock settings
  *           -Definition of RAM sections
  *         [SWS_Mcu_00054]:The structure Mcu_ConfigType shall provide aconfigurable (enable/disable) clock 
  *         failure notification if the MCU provides an interrupt for such detection. If the clock failure 
  *         is detected withother HW mechanisms e.g., the generation of a trap, this notification shall 
  *         be disabled and the failure reporting shall be doneoutside the MCU driver.
  *         [SWS_Mcu_00035]:The definitions for each MCU mode within the structure Mcu_ConfigTypeshall 
  *         contain: (depending on MCU)
  *           -MCU specific properties
  *           -Change of CPU clockChange of Peripheral clock
  *           -Change of PLL settings
  *           -Change of MCU power supply
  *         [SWS_Mcu_00031]:The definitions for each Clock setting within the structure Mcu_ConfigType 
  *         shall contain:
  *           -MCU specific properties as, e.g., clock safety features and special clock distribution settings
  *           -PLL settings /start lock options
  *           -Internal oscillator setting
  *         [SWS_Mcu_00030]:The definitions for each RAM section within the structure Mcu_ConfigType 
  *         shall contain:
  *           -RAM section base address
  *           -Section size
  *           -Data pre-setting to be initialized
  *           -RAM write size     I
  */
typedef struct Mcu_ConfigType_Tag
{


}Mcu_ConfigType;

/** 
  * @brief  Specifies the identification (ID) for a clock setting, which is configured in the 
  *         configuration structure
  *         [SWS_Mcu_00232]:The type Mcu_ClockType defines the identification(ID) for clock 
  *         setting configured via the configuration structure.
  *         [SWS_Mcu_00233]:The type shall be uint8, uint16 or uint32, depending on uC platform.
  */
typedef uint32          Mcu_ClockType;

/** 
  * @brief  This type specifies the reset reason in raw register format read from a reset  
  *         status register.
  *         [SWS_Mcu_00235]:The type Mcu_RawResetType specifies the reset reason in raw register format, 
  *         read from a reset status register.
  *         [SWS_Mcu_00236]:The type shall be uint8, uint16 or uint32based on best performance.
  */
typedef uint32          Mcu_RawResetType;

/** 
  * @brief  This type specifies the identification (ID) for a MCU mode, which is configured 
  *         in the configuration structure.
  *         [SWS_Mcu_00237]:The Mcu_ModeType specifies the identification (ID) for a MCU mode, configured 
  *         via configuration structure.
  *         [SWS_Mcu_00238]:The type shall be uint8, uint16 or uint32
  */
typedef uint32          Mcu_ModeType;

/** 
  * @brief  his type specifies the identification (ID) for a RAM section, which is configured 
  *         in the configuration structure.
  *         [SWS_Mcu_00239]:The Mcu_RamSectionType specifies the identification (ID) for a RAM section, 
  *         configured via the configuration structure.
  *         [SWS_Mcu_00240]:The type shall be uint8, uint16 or uint32, based on best performance. 
  */
typedef uint32          Mcu_RamSectionType;


/** 
  * @brief  This is a status value returned by the function Mcu_GetPllStatus of the MCU module.
  *         [SWS_Mcu_00230]:The type Mcu_PllStatusType is the type of the return value of the 
  *         function Mcu_GetPllStatus.
  *         [SWS_Mcu_00231]:The type of Mcu_PllStatusType is an enumeration withthe following 
  *         values: MCU_PLL_LOCKED, MCU_PLL_UNLOCKED, MCU_PLL_STATUS_UNDEFINED.
  */
typedef enum Mcu_PllStatusType_Tag
{
    MCU_PLL_LOCKED = 0u,
    MCU_PLL_UNLOCKED,
    MCU_PLL_STATUS_UNDEFINED
}Mcu_PllStatusType;

/** 
  * @brief  This is the type of the reset enumerator containing the subset of reset types. 
  *         It is not required that all reset types are supported by hardware.
  *         [SWS_Mcu_00234]:The type Mcu_ResetType, represents the different reset that a specified 
  *         MCU can have.
  *         [SWS_Mcu_00134]:The MCU module shall provide at least the values MCU_POWER_ON_RESET and 
  *         MCU_RESET_UNDEFINED for the enumeration Mcu_ResetType.
  *         Note: Additional reset types of Mcu_ResetType may be added depending on MCU.
  */
typedef enum Mcu_ResetType_Tag
{
    MCU_POWER_ON_RESET = 0u,
    MCU_WATCHDOG_RESET,
    MCU_SW_RESET,
    MCU_RESET_UNDEFINED
}Mcu_ResetType;

/** 
  * @brief  This is the Ram State data type returned by the function Mcu_GetRamState of the Mcu module. 
  *         It is not required that all RAM state types are supported by the hardware.
  */
typedef enum Mcu_RamStateType_Tag
{
    MCU_RAMSTATE_INVALID = 0u,
    MCU_RAMSTATE_VALID
}Mcu_RamStateType;


extern void Mcu_Init( const Mcu_ConfigType *ConfigPtr );
extern Std_ReturnType Mcu_InitRamSection( Mcu_RamSectionType RamSection );
extern Std_ReturnType Mcu_InitClock( Mcu_ClockType ClockSetting );
extern Std_ReturnType Mcu_DistributePllClock( void );
extern Mcu_PllStatusType Mcu_GetPllStatus( void );
extern Mcu_ResetType Mcu_GetResetReason( void );
extern Mcu_RawResetType Mcu_GetResetRawValue( void );
extern void Mcu_PerformReset( void );
extern void Mcu_SetMode( Mcu_ModeType McuMode );
extern void Mcu_GetVersionInfo( Std_VersionInfoType *versioninfo );
extern Mcu_RamStateType Mcu_GetRamState( void );


#endif /* MCU_H */

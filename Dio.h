/**
  * @file    Dio.h
  * @author  Modular Ninja Team
  * @brief   Specification of Dio Driver
  * https://www.autosar.org/fileadmin/user_upload/standards/classic/20-11/AUTOSAR_SWS_StandardTypes.pdf
*/
#ifndef DIO_H
#define DIO_H

/** 
  * @brief  Numeric ID of a DIO channel.
  *         [SWS_Dio_00015]:Parameters of type Dio_ChannelType contain the numeric ID of a DIO channel.
  *         [SWS_Dio_00180]:The mapping of the ID is implementation specific but not configurable.
  *         [SWS_Dio_00017]:For parameter values of type Dio_ChannelType, the Dio’s user shall use 
  *         the symbolic names providedby the configuration description.
  * 
  * @range  Shall cover all available DIO channels
  */
typedef uint32          Dio_ChannelType; 

/** 
  * @brief  Numeric ID of a DIO port
  *         [SWS_Dio_00018]:Parameters of type Dio_PortTypecontain the numeric ID of a DIO port.
  *         [SWS_Dio_00181]:The mapping of ID is implementation specific but not configurable.
  *         [SWS_Dio_00020]⌈For parameter values of type Dio_PortType, the user shall use the symbolic 
  *         names provided by the configuration description.
  * 
  * @range  Shall cover all available DIO Ports.
  */
typedef uint32          Dio_PortType;


/** 
  * @brief  Type for the definition of a channel group, which consists of several adjoining channels 
  *         within a port.
  *         [SWS_Dio_00021]:Dio_ChannelGroupTypeis the type for the definition of a channel group, 
  *         which consists of several adjoining channels within a port.
  *         [SWS_Dio_00022]:For parameter values of type Dio_ChannelGroupType, the user shall use 
  *         the symbolic names provided by the configuration description.
  * 
  * @range  Shall cover all available DIO Ports.
  */
typedef struct  Dio_ChannelGroupType_Tag
{
    uint32          mask;   /**< This element mask which defines the positions of the channel group */
    uint32          offset; /**< This element shall be the position of the Channel Group on the port, 
                            counted from the LSB */
    Dio_PortType    port;   /**< This shall be the port on which the Channel group is defined. */
}Dio_ChannelGroupType;

/** 
  * @brief  These are the possible levels a DIO channel can have (input or output)
  *         [SWS_Dio_00023]:Dio_LevelTypeis the type for the possible levels that a DIO channel can 
  *         have (input or output).
  * 
  * @range  STD_LOW, STD_HIGH
  */
typedef uint8           Dio_LevelType;

/** 
  * @brief  If the μC owns ports of different port widths (e.g. 4, 8,16...Bit) Dio_PortLevelType 
  *         inherits the size of the largest port.
  *         [SWS_Dio_00024]⌈Dio_PortLevelTypeis the type for the value of a DIO port.
  * 
  * @range  0...xxx
  */
typedef uint32          Dio_PortLevelType;


extern Dio_LevelType Dio_ReadChannel( Dio_ChannelType ChannelId );
extern void Dio_WriteChannel( Dio_ChannelType ChannelId, Dio_LevelType Level );
extern void Dio_WriteChannel( Dio_ChannelType ChannelId, Dio_LevelType Level );
extern Dio_PortLevelType Dio_ReadPort( Dio_PortType PortId );
extern void Dio_WritePort( Dio_PortType PortId, Dio_PortLevelType Level );
extern Dio_PortLevelType Dio_ReadChannelGroup( const Dio_ChannelGroupType *ChannelGroupIdPtr );
extern void Dio_WriteChannelGroup( const Dio_ChannelGroupType *ChannelGroupIdPtr, Dio_PortLevelType Level );
extern void Dio_MaskedWritePort( Dio_PortType PortId, Dio_PortLevelType Level, Dio_PortLevelType Mask );

#endif /* DIO_H */

/**
  * @file    Mcu.h
  * @author  Modular Ninja Team
  * @brief   Specification of MCU Driver
  * https://www.autosar.org/fileadmin/user_upload/standards/classic/20-11/AUTOSAR_SWS_StandardTypes.pdf
*/
#include "Std_Types.h"
#include "Mcu.h"

/**
  * @brief  This service initializes the MCU driver. 
  *         [SWS_Mcu_00026]:The function Mcu_Init shall initialize the MCU module, i.e. make the 
  *         configuration settings for power down, clock and RAM sections visible within the MCU module.
  *         The MCU module’s implementer shall apply the following rules regarding initialization of 
  *         controller registers within the function Mcu_Init:1.
  *         [SWS_Mcu_00116]:If the hardware allows for only one usage of the register, the driver module 
  *         implementing that functionality is responsible for initializing the register.
  *         [SWS_Mcu_00244]:If the register can affect several hardware modules and if it is an I/O register, 
  *         it shall be initialised by the PORT driver.
  *         [SWS_Mcu_00245]:If the register can affect severalhardware modules and if it is not an I/O 
  *         register, it shall be initialised by this MCU driver.
  *         [SWS_Mcu_00246]:One-time writable registers that require initialisation directly after reset 
  *         shall be initialised by the startup code.
  *         [SWS_Mcu_00247]:All other registers not mentioned before shall be initialised by the start-up code.
  *         Note: After the execution of the function Mcu_Init, the configuration data are accessible
  *         and can be used by the MCU module functions as, e.g., Mcu_InitRamSection.
  * 
  * @param  ConfigPtr Pointer to MCU driver configuration set.
  * 
  * @retval None
  */
void Mcu_Init( const Mcu_ConfigType *ConfigPtr )
{

}

/**
  * @brief  This service initializes the RAM section wise.
  *         [SWS_Mcu_00011]:The function Mcu_InitRamSection shallfill the memory from address 
  *         McuRamSectionBaseAddress up to address McuRamSectionBaseAddress + McuRamSectionSize - 1 
  *         with the byte-value contained in McuRamDefaultValue and by writing at once a number of 
  *         bytes defined by McuRamSectionWriteSize, where McuRamSectionBaseAddress, McuRamSectionSize, 
  *         McuRamDefaultValue and McuRamSectionWriteSize are the values of the configuration parameters 
  *         for each RamSection(see SWS_Mcu_00030)
  *         [SWS_Mcu_00136]:The MCU module’s environment shall call the function Mcu_InitRamSection only 
  *         after the MCU module has been initialized using the function Mcu_Init.
  * 
  * @param  RamSection Selects RAM memory section provided in configuration set.
  *
  * @retval E_OK: command has been accepted
  *         E_NOT_OK: command has not been accepted e.g. due to parameter error
  */
Std_ReturnType Mcu_InitRamSection( Mcu_RamSectionType RamSection )
{
    return E_OK;
}

/**
  * @brief  This service initializes the PLL and other MCU specific clock options.
  *         [SWS_Mcu_00137]:The function Mcu_InitClockshall initialize the PLL and other MCU specific 
  *         clock options. The clock configuration parameters are provided via the configuration structure.
  *         [SWS_Mcu_00138]:The function Mcu_InitClock shall start the PLL lock procedure (if PLL shall 
  *         be initialized) and shall return without waiting until the PLL is locked.
  *         [SWS_Mcu_00139]:The MCU module’s environment shall only callthe function Mcu_InitClockafter 
  *         the MCU module has been initialized using the function Mcu_Init.
  *         [SWS_Mcu_00210]:The function Mcu_InitClock shall be disabled if the parameter McuInitClockis 
  *         set to FALSE. Instead this function is available if the former parameter is set to TRUE
  * 
  * @param  ClockSetting Selects RAM memory section provided in configuration set.
  *
  * @retval E_OK: command has been accepted
  *         E_NOT_OK: command has not been accepted e.g. due to parameter error
  */
Std_ReturnType Mcu_InitClock( Mcu_ClockType ClockSetting )
{
    return E_OK;
}

/**
  * @brief  This service activates the PLL clock to the MCU clock distribution
  *         [SWS_Mcu_00140]: The function Mcu_DistributePllClockshall activate the PLL clock to the 
  *         MCU clock distribution.
  *         [SWS_Mcu_00141]: The function Mcu_DistributePllClockshall remove the current clock source 
  *         (for example internal oscillator clock) from MCU clock distribution. The MCU module’s 
  *         environment shall only call the function Mcu_DistributePllClockafter the status of the PLL 
  *         has been detected as locked by the function Mcu_GetPllStatus.
  *         [SWS_Mcu_00056]: The function Mcu_DistributePllClock shall returnwithout affecting the MCU 
  *         hardware if the PLL clock has been automatically activated by the MCU hardware.
  *         [SWS_Mcu_00142]: If the function Mcu_DistributePllClockis called before PLL has locked, this 
  *         function shall return E_NOT_OK immediately, without any further action.
  *         [SWS_Mcu_00205]:ThefunctionMcu_DistributePllClock shall be available if the pre-compile parameter 
  *         McuNoPll is set to FALSE. Otherwise, this Api has to be disabled(see also ECUC_Mcu_00180 : ).
  *          
  * @param  None.
  *
  * @retval E_OK: command has been accepted
  *         E_NOT_OK: command has not been accepted e.g. due to parameter error
  */
Std_ReturnType Mcu_DistributePllClock( void )
{
    return E_OK;
}

/**
  * @brief  This service provides the lock status of the PLL.
  *         [SWS_Mcu_00008]:The function Mcu_GetPllStatus shall return the lock status of the PLL.
  *         [SWS_Mcu_00132]:The function Mcu_GetPllStatus shall return MCU_PLL_STATUS_UNDEFINED 
  *         if this function is called prior to calling of the function Mcu_Init.
  *         [SWS_Mcu_00206]:The function Mcu_GetPllStatus shall also return MCU_PLL_STATUS_UNDEFINED 
  *         if the pre-compile parameterMcuNoPll is set to TRUE.
  *          
  * @param  None.
  *
  * @retval Mcu_PllStatusType
  */
Mcu_PllStatusType Mcu_GetPllStatus( void )
{
    return MCU_PLL_STATUS_UNDEFINED;
}

/**
  * @brief  The service reads the reset type from the hardware, if supported.
  *         [SWS_Mcu_00005]:The function Mcu_GetResetReason shall read the reset reason from the 
  *         hardware and return this reason if supported by the hardware. If the hardware does not 
  *         support the hardware detection of the reset reason, the return value from the function 
  *         Mcu_GetResetReason shall always be MCU_POWER_ON_RESET.
  *         [SWS_Mcu_00133]:The function Mcu_GetResetReason shall return MCU_RESET_UNDEFINED if this 
  *         function is called prior to calling of the function Mcu_Init, and if supported by the hardware.
  *         The User should ensure that the reset reason is cleared once it has been read out to avoid 
  *         multiple reset reasons.
  *         Note: In case of multiple calls to this function the return value should always be the same.
  *          
  * @param  None.
  *
  * @retval Mcu_ResetType
  */
Mcu_ResetType Mcu_GetResetReason( void )
{
    return MCU_POWER_ON_RESET;
}

/**
  * @brief  The service reads the reset type from the hardware register, if supported
  *         [SWS_Mcu_00135]:Thefunction Mcu_GetResetRawValue shall return an implementation specific 
  *         value which does not correspond to a valid value of the reset status register and is not 
  *         equal to 0 if this function is called prior to calling of the function Mcu_Init, and if 
  *         supported by the hardware.
  *         [SWS_Mcu_00006]:The function Mcu_GetResetRawValue shall read the reset raw value from the 
  *         hardware register if the hardware supports this. If the hardware does not have a reset 
  *         status register, the return value shall be 0x0.
  *         The User should ensure that the reset reason is cleared once it has been read out to avoid 
  *         multiple reset reasons.
  *         Note: In case of multiple calls to this function the return value should always be the same
  *          
  * @param  None.
  *
  * @retval Mcu_RawResetType
  */
Mcu_RawResetType Mcu_GetResetRawValue( void )
{
    return 0u;
}

/**
  * @brief  The service performs a microcontroller reset.
  *         [SWS_Mcu_00143]:The function Mcu_PerformReset shall perform a microcontroller reset by 
  *         using the hardware feature of the microcontroller.
  *         [SWS_Mcu_00144]:The function Mcu_PerformReset shall perform the reset type which is 
  *         configured in the configuration set.
  *         [SWS_Mcu_00145]:The MCU module’s environment shall only call the function Mcu_PerformReset 
  *         after the MCU module has been initialized by the function Mcu_Init.
  *         [SWS_Mcu_00146]:The function Mcu_PerformReset is only available if the pre-compile parameter 
  *         McuPerformResetApi is set to TRUE. If set to FALSE, the functionMcu_PerformReset is not 
  *         applicable.
  *          
  * @param  None.
  *
  * @retval None
  */
void Mcu_PerformReset( void )
{

}

/**
  * @brief  This service activates the MCU power modes.
  *         [SWS_Mcu_00147]:The function Mcu_SetModeshall set the MCU power mode. In case of CPU power 
  *         down mode, the function Mcu_SetModereturns after it has performed a wake-up
  *         [SWS_Mcu_00148]:The MCU module’s environment shallonly call the function Mcu_SetMode after 
  *         the MCU module has been initialized by the function Mcu_Init.
  *         Note: The environment of the function Mcu_SetMode has to ensure that the ECU is ready for 
  *         reduced power mode activation.
  *         Note:The API Mcu_SetMode assumes that all interrupts are disabled prior the call of the 
  *         API by the calling instance. The implementation has to take care that no wakeup interrupt 
  *         event is lost. This could be achieved by a check whether pending wakeup interrupts already 
  *         have occurred even if Mcu_SetModehasnot set the controllerto power down mode yet. 
  *          
  * @param  McuMode. Set different MCU power modes configured in the configuration set
  *
  * @retval None
  */
void Mcu_SetMode( Mcu_ModeType McuMode )
{

}

/**
  * @brief  This service returns the version information of this module
  *          
  * @param  versioninfo. Pointer to where to store the version information of this module
  *
  * @retval None 
  */
void Mcu_GetVersionInfo( Std_VersionInfoType *versioninfo )
{

}

/**
  * @brief  This service provides the actual status of the microcontroller Ram. (if supported)
  *         Note:Some microcontrollers offer the functionality to check if the Ram Status is valid 
  *         after a reset. The function  Mcu_GetRamStatecan be used for this reason. 
  *         [SWS_Mcu_00208]:The MCU module’s environment shall call this function only ifthe MCU module 
  *         has been already initialized using the function MCU_Init.
  *         [SWS_Mcu_00209]:The function  Mcu_GetRamState shall be available to the user if the pre-compile 
  *         parameter McuGetRamStateApi is set to TRUE. Instead, if the former parameter isset to FALSE, 
  *         this function shall be disabled(e.g. the hardware does not support this functionality).
  *          
  * @param  None.
  *
  * @retval Mcu_RamStateType Status of the Ram Content
  */
Mcu_RamStateType Mcu_GetRamState( void )
{
    return MCU_RAMSTATE_VALID;
}

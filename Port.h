/**
  * @file    Port.h
  * @author  Modular Ninja Team
  * @brief   Specification of Port Driver
  * https://www.autosar.org/fileadmin/user_upload/standards/classic/20-11/AUTOSAR_SWS_StandardTypes.pdf
*/
#ifndef PORT_H
#define PORT_H

/** 
  * @brief  Type of the external data structure containing the initialization data for this module.
  *         [SWS_Port_00073]:The type Port_ConfigType is a type for the external data structure containing 
  *         the initialization data for the PORT Driver.
  *         Note: The user shall use the symbolic names defined in the configuration tool.
  *         Note: The configuration of each port pin is MCU specific. Therefore, it is not possible to 
  *         include a complete list of different configurations in this specification.
  *         [SWS_Port_00072]:A list of possible port configurations for the structure Port_ConfigType 
  *         is given below:
  *             Pin mode (e.g. DIO, ADC, SPI ...)–this port pin configuration is mandatory unless the 
  *             port pin is configured for DIO.
  *             Pin direction (input, output) –this port pin configuration is mandatory when the port 
  *             pin is to be used for DIO.
  *             Pin level init value (see SWS_Port_00055) –this port pin configuration is mandatory 
  *             when the port pin is used for DIO.
  *             Pin direction changeable during runtime (STD_ON/STD_OFF) –this port pin configuration 
  *             is MCU dependent.
  *             Pin mode changeable during runtime (STD_ON/STD_OFF) –configuration is MCU dependent.
  *             Optional parameters (if supported by hardware) 
  *             - Slew rate control
  *             - Activation of internal pull-ups.
  *             - Microcontroller specific port pin properties.     I
  */
typedef struct Port_ConfigType_Tag
{

}Port_ConfigType;

/** 
  * @brief  Data type for the symbolic name of a port pin.
  *         [SWS_Port_00013]:The type Port_PinType shall be used for the symbolic name of a Port Pin.
  *         [SWS_Port_00219]:The type Port_PinType shall be uint8, uint16 or uint32 based on the specific 
  *         MCU platform.
  *         Note: The user shall use the symbolic names provided by the configuration tool.
  * @range  Shall cover all available port pins. The type should be chosen for the specific MCU platform.
  */
typedef uint32          Port_PinType;

/** 
  * @brief  Possible directions of a port pin.
  *         [SWS_Port_00046]:The type Port_PinDirectionType is a type for defining the direction of a 
  *         Port Pin.
  *         [SWS_Port_00220]:The type Port_PinDirectionType shall be of enumeration type having range 
  *         as PORT_PIN_IN and PORT_PIN_OUT.
  */
typedef enum Port_PinDirectionType_Tag
{
    PORT_PIN_IN = 0u,
    PORT_PIN_OUT
}Port_PinDirectionType;

/** 
  * @brief  Different port pin modes
  *         [SWS_Port_00124]:A port pin shall be configurable with a number of port pin modes 
  *         (type Port_PinModeType). 
  *         [SWS_Port_00212]:The type Port_PinModeType shall be used with the function call Port_SetPinMode
  *         [SWS_Port_00221]:The type Port_PinModeType shall be uint8, uint16 or uint32.
  * @range  As several port pin modes shall be configurable on one pin, the range shall be determined by 
  *         the implementation
  */
typedef uint32          Port_PinModeType;


extern void Port_Init( const Port_ConfigType *ConfigPtr );
extern void Port_SetPinDirection( Port_PinType Pin, Port_PinDirectionType Direction );
extern void Port_RefreshPortDirection( void );
extern void Port_GetVersionInfo( Std_VersionInfoType *versioninfo );
extern void Port_SetPinMode( Port_PinType Pin, Port_PinModeType Mode );

#endif /* PORT_H */

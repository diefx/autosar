/**
  * @file    Dio.h
  * @author  Modular Ninja Team
  * @brief   Specification of Dio Driver
  * https://www.autosar.org/fileadmin/user_upload/standards/classic/20-11/AUTOSAR_SWS_StandardTypes.pdf
*/
#include "Std_Types.h"
#include "Dio.h"


/**
  * @brief  Returns the value of the specified DIO channel.
  *         [SWS_Dio_00027]:The Dio_ReadChannelfunction shall return the value of the specified DIO channel.
  *         Regarding the return value of the Dio_ReadChannelfunction, the requirements [SWS_Dio_00083]  
  *         and [SWS_Dio_00084] are applicable.Furthermore, the requirements SWS_Dio_00005, SWS_Dio_00118 
  *         and SWS_Dio_00026 are applicable to the Dio_ReadChannelfunction.
  *         [SWS_Dio_00074] :If development error detection is enabled, the services Dio_ReadChannel, 
  *         Dio_WriteChannel and Dio_FlipChannel shall check the “ChannelId” parameter to be valid 
  *         within the current configuration. If the “ChannelId” parameter is invalid, the functions 
  *         shall report the error code DIO_E_PARAM_INVALID_CHANNEL_IDto the DET
  * 
  * @param  ChannelId ID of DIO channel
  * 
  * @retval STD_HIGH The physical level of the corresponding Pin is STD_HIGH
  *         STD_LOW The physical level of the corresponding Pin is STD_LOW
  */
Dio_LevelType Dio_ReadChannel( Dio_ChannelType ChannelId )
{

}

/**
  * @brief  Service to set a level of a channel
  *         [SWS_Dio_00028]:If the specified channel is configured as an output channel, the Dio_WriteChannel
  *         function shall set the specified Level for the specified channel.
  *         [SWS_Dio_00029]:If the specified channel is configured as an input channel, the Dio_WriteChannel
  *         function shall have no influence on the physical output.
  *         [SWS_Dio_00079]:If the specified channel is configured as an input channel, the Dio_WriteChannel
  *         function shall have no influence on the result of the next Read-Service.
  * 
  * @param  ChannelId ID of DIO channel
  * @param  Level Value to be written
  * 
  * @retval None
  */
void Dio_WriteChannel( Dio_ChannelType ChannelId, Dio_LevelType Level )
{

}

/**
  * @brief  Returns the level of all channels of that port.
  *         [SWS_Dio_00031]:The Dio_ReadPortfunction shall return the level of all channels of that port.
  *         [SWS_Dio_00104]:When reading a port which is smaller than the Dio_PortLevelType using the 
  *         Dio_ReadPortfunction, the function shall set the bits corresponding to undefined port pins to 0.
  *         Furthermore, the requirements SWS_Dio_00005, SWS_Dio_00118 and SWS_Dio_00026 are applicable 
  *         to the Dio_ReadPortfunction.
  *         [SWS_Dio_00075]:If developmenterror detection is enabled, the functions Dio_ReadPort, 
  *         Dio_WritePort and Dio_MaskedWritePort shall check the “PortId” parameter to be valid 
  *         within the current configuration. If the “PortId” parameter is invalid, the functions 
  *         shall report the error code DIO_E_PARAM_INVALID_PORT_ID to the DET
  * 
  * @param  ChannelId ID of DIO channel
  * 
  * @retval Dio_PortLevelType Level of all channels of that port
  */
Dio_PortLevelType Dio_ReadPort( Dio_PortType PortId )
{

}

/**
  * @brief  Service to set a value of the port.
  *         [SWS_Dio_00034]:The Dio_WritePortfunction shall set the specified value for the specified port.
  *         [SWS_Dio_00035]:When the Dio_WritePort function is called, DIO Channels that are configured as 
  *         input shall remain unchanged.
  *         [SWS_Dio_00105]:When writing a port which is smaller than the Dio_PortLevelType using the 
  *         Dio_WritePort function, the function shall ignore the MSB.
  *         [SWS_Dio_00108]:The Dio_WritePortfunction shall have no effect on channels within this port 
  *         which are configured as input channels. 
  * 
  * @param  ChannelId ID of DIO channel
  * @param  Level Value to be written
  * 
  * @retval None
  */
void Dio_WritePort( Dio_PortType PortId, Dio_PortLevelType Level )
{

}

/**
  * @brief  This Service reads a subset of the adjoining bits of a port.
  *         [SWS_Dio_00037]:The Dio_ReadChannelGroup function shall read a subset of the adjoining 
  *         bits of a port (channel group).
  *         [SWS_Dio_00092]:The Dio_ReadChannelGroup function shall do the masking of the channel 
  *         group.
  *         [SWS_Dio_00093]:The Dio_ReadChannelGroup function shall do the shifting so that the values 
  *         read by the function are aligned to the LSB.
  *         [SWS_Dio_00114]:If development error detection is enabled, the functions Dio_ReadChannelGroup 
  *         and Dio_WriteChannelGroup shall check the “ChannelGroupIdPtr” parameter to be valid within 
  *         the current configuration. If the “ChannelGroupIdPtr” parameter is invalid, the functions 
  *         shall report the error code DIO_E_PARAM_INVALID_GROUPto the DET.
  * 
  * @param  ChannelGroupIdPtr Pointer to ChannelGroup
  * 
  * @retval Dio_PortLevelType Level of a subset of the adjoining bits of a port
  */
Dio_PortLevelType Dio_ReadChannelGroup( const Dio_ChannelGroupType *ChannelGroupIdPtr )
{

}

/**
  * @brief  Service to set a subset of the adjoining bits of a port to a specified level.
  *         [SWS_Dio_00039]:The Dio_WriteChannelGroup function shall set a subset of the adjoining 
  *         bits of a port (channel group) to a specified level
  *         [SWS_Dio_00040]:The Dio_WriteChannelGroup shall not change the remaining channels of the 
  *         port and channels which are configured as input.
  *         [SWS_Dio_00090]:The Dio_WriteChannelGroup function shall do the masking of the channel group.
  *         [SWS_Dio_00091]:The function Dio_WriteChannelGroup shall do the shifting so that the values 
  *         written by the function are aligned to the LSB.
  * 
  * @param  ChannelGroupIdPtr Pointer to ChannelGroup
  * @param  Level Value to be written
  * 
  * @retval Dio_PortLevelType Level of a subset of the adjoining bits of a port
  */
void Dio_WriteChannelGroup( const Dio_ChannelGroupType *ChannelGroupIdPtr, Dio_PortLevelType Level )
{

}

/**
  * @brief  Returns the version information of this module.
  *         [SWS_Port_00225]:if Det is enabled, the parameter versioninfo shall be checked for being NULL. 
  *         The error DIO_E_PARAM_POINTER shall be reported in case the value is a NULL pointer.
  * 
  * @param  versioninfo. Pointer to where to store the version information of this module
  *
  * @retval None 
  */
void Dio_GetVersionInfo( Std_VersionInfoType *versioninfo )
{

}

/**
  * @brief  Service to flip (change from 1 to 0 or from 0 to 1) the level of a channel and return 
  *         the level of the channel after flip
  *         [SWS_Dio_00191]:If the specified channel is configured as an output channel, the Dio_FlipChannel
  *         function shall read levelof the channel (requirements [SWS_Dio_00083]& [SWS_Dio_00084] are 
  *         applicable) and invert it, then write the inverted levelto the channel.The return value 
  *         shall be the inverted level of the specified channel.
  *         [SWS_Dio_00192]:If the specified channel is configured as an input channel, the Dio_FlipChannel
  *         function shall have no influence on the physical output. The return value shall be the 
  *         level of the specified channel.
  *         [SWS_Dio_00193]:If the specified channel is configured as an input channel, the Dio_FlipChannel
  *         function shall have no influence on the result of the next Read-Service
  * 
  * @param  ChannelId ID of DIO channel
  *
  * @retval STD_HIGH The physical level of the corresponding Pin is STD_HIGH
  *         STD_LOW The physical level of the corresponding Pin is STD_LOW
  */
Dio_LevelType Dio_FlipChannel( Dio_ChannelType ChannelId )
{

}

/**
  * @brief  Service to set the value of a given port with required mask.
  *         [SWS_Dio_00202]:The Dio_MaskedWritePort function shall set the specified value for the 
  *         channels in the specified port if the corresponding bit in Mask is '1'.
  *         [SWS_Dio_00203]:When the Dio_MaskedWritePort function is called, DIO Channels that are 
  *         configured as input shall remain unchanged.
  *         [SWS_Dio_00204]:When writing a port which is smaller than the Dio_PortLevelType using 
  *         the Dio_MaskedWritePort function (see [SWS_Dio_00103]), the function shall ignore the MSB.
  * 
  * @param  PortId ID of DIO port
  * @param  Level Value to be written
  * @param  Mask Channels to be masked in the port
  *
  * @retval None
  */
void Dio_MaskedWritePort( Dio_PortType PortId, Dio_PortLevelType Level, Dio_PortLevelType Mask )
{

}

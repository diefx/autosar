/**
  * @file    Std_Types.h
  * @author  Modular Ninja Team
  * @brief   Specification of Standard Types
  * https://www.autosar.org/fileadmin/user_upload/standards/classic/20-11/AUTOSAR_SWS_StandardTypes.pdf
*/

#ifndef STD_TYPES_H
#define STD_TYPES_H

#include "Platform_Types.h"

/** 
  * @brief  This type can be used as standard API return type which is shared between the RTE 
  *         and the BSW modules.
  * @range  E_OK and E_NOT_OK           I
  */
typedef uint8       Std_ReturnType;

/**
  * @brief  The type Std_TransformerClass shall be an enumeration with the following elements 
  *         where each element represents a transformer class:
  * @range  STD_TRANSFORMER_UNSPECIFIED, STD_TRANSFORMER_SERIALIZER, STD_TRANSFORMER_SAFETY
  *         STD_TRANSFORMER_SECURITY and STD_TRANSFORMER_CUSTOM           I
  */
typedef uint8       Std_TransformerClass;

/**
  * @brief  This type is used to encode the different type of messages. -Currently this encoding 
  *         is limited to the distinction between requests and responses in C/S communication.
  * @range  STD_MESSAGETYPE_REQUEST and STD_MESSAGETYPE_RESPONSE        I
  */
typedef uint8       Std_MessageTypeType;

/**
  * @brief  This type is used to encode different types of results for response messages. Currently 
  *         this encoding is limited to the distinction between OK and ERROR responses.
  * @range  STD_MESSAGERESULT_OK and STD_MESSAGERESULT_ERROR        I
  */
typedef uint8       Std_MessageResultType;

/**
  * @brief Type for the function pointer to extract the relevant protocol header fields of the 
  *        message and the type of the message result of a transformer. At the time being, this 
  *        is limited to the types used for C/S communication (i.e., REQUEST and RESPONSE and 
  *        OK and ERROR).
  * @range STD_MESSAGERESULT_OK and STD_MESSAGERESULT_ERROR        I
  */
Std_ReturnType (*Std_ExtractProtocolHeaderFieldsType)( const uint8 *buffer, uint32 bufferLength, Std_MessageTypeType *messageType, Std_MessageResultType *messageResult );

/**
  * @brief This type shall be used to request the version of a BSW module using the 
  *        <Module name>_GetVersionInfo() function    
  */
typedef struct Std_VersionInfoType_Tag
{
    uint16      vendorID;           /**< enum value 1 */
    uint16      moduleID;           /**< enum value 1 */
    uint8       sw_major_version;   /**< enum value 1 */
    uint8       sw_minor_version;   /**< enum value 1 */
    uint8       sw_patch_version;   /**< enum value 1 */
}Std_VersionInfoType;

/**
  * @brief  The Std_TransformerErrorCode represents a transformer error in the context of a 
  *         certain transformer chain    
  */
typedef struct Std_TransformerError_Tag
{
    uint8                   errorCode;
    Std_TransformerClass    transformerClass;
}Std_TransformerError;

    
#ifndef STATUSTYPEDEFINED
#define STATUSTYPEDEFINED
    #define E_OK    0u
    typedef unsigned char StatusType; /* OSEK compliance */
#endif
#define E_NOT_OK    1u

#define STD_HIGH    1u /* Physical state 5V or 3.3V */
#define STD_LOW     0u /* Physical state 0V */

#define STD_ACTIVE  1u /* Logical state active */
#define STD_IDLE    0u /* Logical state idle */

#define STD_ON      1u
#define STD_OFF     0u

#endif /* STD_TYPES_H */

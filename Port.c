/**
  * @file    Port.c
  * @author  Modular Ninja Team
  * @brief   Specification of Port Driver
  * https://www.autosar.org/fileadmin/user_upload/standards/classic/20-11/AUTOSAR_SWS_StandardTypes.pdf
*/
#include "Std_Types.h"
#include "Port.h"


/**
  * @brief  Initializes the Port Driver module.
  *         [SWS_Port_00041]:The function Port_Init shall initialize ALL ports and port pins with the 
  *         configuration set pointed to by the parameter ConfigPtr.
  *         [SWS_Port_00078]:The Port Driver module’s environment shall call the function Port_Init 
  *         first in order to initialize the port for use.
  *         [SWS_Port_00213]:If Port_Initfunction is not called first, then no operation can occur 
  *         on the MCU ports and port pins.
  *         [SWS_Port_00042]:The function Port_Init shall initialize all configured resources.
  *         The function Port_Init shall apply the following rules regarding initialisation of controller 
  *         registers:
  *             [SWS_Port_00113] If the hardware allows for only one usage of the register, the driver 
  *             module implementing that functionalityis responsible for initializing  the register.
  *             [SWS_Port_00214] If the register can affect several hardware modules and if it is an I/O 
  *             register it shall be initialised by this PORT driver.
  *             [SWS_Port_00215] If theregister can affect several hardware modules and if it is not an 
  *             I/O  register, it shall be initialised by the MCU driver.
  *             [SWS_Port_00217] One-time writable registers that require initialisation directly after 
  *             reset shall be initialised by the startup code.
  *             [SWS_Port_00218] All the other registersnot mentioned before, shall be initialised by 
  *             the start-up code.
  *         [SWS_Port_00043]:The function Port_Init shall avoid glitches and spikes on the affected 
  *         port pins.
  *         [SWS_Port_00071]:The Port Driver module’s environment shall call the function Port_Init 
  *         after a reset in order to reconfigure the ports and port pins of the MCU.
  *         [SWS_Port_00002]:The function Port_Init shall initialize all variables used by the PORT 
  *         driver module to an initial state.
  *         [SWS_Port_00003]:The Port Driver module’s environment may also uses the function Port_Init 
  *         to initialize the driver software and reinitialize the ports and port pins to another 
  *         configured state depending on the configuration set passed to this function.
  *         Note: In some cases, MCU port control hardware provides an output latch for setting the 
  *         output level on a port pin that may be used as a DIO port pin.
  *         [SWS_Port_00055]:The function Port_Init shall set the port pin output latch to a default 
  *         level (defined during configuration) before setting the port pin direction to output.
  *         [SWS_Port_00121]:The function Port_Init shall always have a pointer as a parameter, even 
  *         though for the configuration variant VARIANT-PRE-COMPILE, no configuration set shall be 
  *         given. In this case, the Port Driver module’s environment shall pass a NULL pointer to 
  *         the function Port_Init.
  * 
  * @param  ConfigPtr Pointer to configuration set.
  * 
  * @retval None
  */
void Port_Init( const Port_ConfigType *ConfigPtr )
{

}

/**
  * @brief  Sets the port pin direction.
  *         [SWS_Port_00063]:The function Port_SetPinDirection shall set the port pin direction during 
  *         runtime.
  *         [SWS_Port_00054]:The function Port_SetPinDirection shall be re-entrant if accessing different 
  *         pins independent of a port.
  *         [SWS_Port_00086]:The function Port_SetPinDirection shall only be availableto the userif the 
  *         pre-compileparameter PortSetPinDirectionApi is set to TRUE. If set to FALSE, the function 
  *         Port_SetPinDirection is not available.
  * 
  * @param  Pin Port Pin ID number
  * @param  Direction Port Pin direction
  * 
  * @retval None
  */
void Port_SetPinDirection( Port_PinType Pin, Port_PinDirectionType Direction )
{

}

/**
  * @brief  Refreshes port direction.
  *         [SWS_Port_00060]:The function Port_RefreshPortDirection shall refresh the direction of 
  *         all configured ports to the configured direction
  *         [SWS_Port_00061]:The function Port_RefreshPortDirection shall exclude those port pins 
  *         from refreshing that are configured as ‘pin direction changeable during runtime‘.
  *         The configuration tool shall provide names for each configured port pin. 
  * 
  * @param  None
  * 
  * @retval None
  */
void Port_RefreshPortDirection( void )
{

}

/**
  * @brief  Returns the version information of this module.
  *         [SWS_Port_00225]:if Det is enabled, the parameter versioninfo shall be checked for being NULL. 
  *         The error PORT_E_PARAM_POINTER shall be reported in case the value is a NULL pointer.
  * 
  * @param  versioninfo. Pointer to where to store the version information of this module
  *
  * @retval None 
  */
void Port_GetVersionInfo( Std_VersionInfoType *versioninfo )
{

}

/**
  * @brief  Sets the port pin mode.
  *         [SWS_Port_00125]:The function Port_SetPinMode shall set the port pin mode of the referenced 
  *         pin during runtime.
  *         [SWS_Port_00128]:The function Port_SetPinMode shall be re-entrant if accessing different pins, 
  *         independent of a port.
  *         [SWS_Port_00223]:If Det is enabled, the function Port_SetPinMode shall report PORT_E_MODE_UNCHANGEABLE 
  *         error and return without any other action, if the parameter PortPinModeChangeable is set to FALSE.
  *         Configuration of Port_SetPinMode: All ports and port pins shall be configured by the configuration tool
  * 
  * @param  Pin. Port Pin ID number
  * @param  Mode. New Port Pin mode to be set on port pin
  *
  * @retval None 
  */
void Port_SetPinMode( Port_PinType Pin, Port_PinModeType Mode )
{

}

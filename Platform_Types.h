/**
  * @file    Platform_Types.h
  * @author  Modular Ninja Team
  * @brief   Specification of Platform Types
  * https://www.autosar.org/fileadmin/user_upload/standards/classic/20-11/AUTOSAR_SWS_PlatformTypes.pdf
  */

#ifndef PLATFORM_TYPES_H
#define PLATFORM_TYPES_H


/** @defgroup   CPU Types
  * @brief      According to the register width of the CPU used
  * @{
  */
#define CPU_TYPE_8              8
#define CPU_TYPE_16             16
#define CPU_TYPE_32             32
#define CPU_TYPE_64             64
/**
  * @}
  */


/** @defgroup   Endianess
  * @brief      The pattern for bit, byte and word ordering in native types.
  * @{
  */
#define MSB_FIRST               0
#define LSB_FIRST               1
#define HIGH_BYTE_FIRST         0
#define LOW_BYTE_FIRST          1
/**
  * @}
  */


/** @defgroup   Specific Platform definitions
  * @brief      platform specification for ARM stm32fxxx microcontrollers.
  * @{
  */
#define CPU_TYPE                CPU_TYPE_32
#define CPU_BIT_ORDER           LSB_FIRST
#define CPU_BYTE_ORDER          LOW_BYTE_FIRST
/**
  * @}
  */

/** @defgroup   Optimized integer data types
  * @brief      AUTOSAR stadarization of data types to avoid compiler dependency.
  * @{
  */
typedef unsigned char           boolean;

typedef unsigned char           uint8;
typedef unsigned short          uint16;
typedef unsigned int            uint32;
typedef unsigned long long      uint64;

typedef signed char             sint8;
typedef signed short            sint16;
typedef signed int              sint32;
typedef signed long long        sint64;

typedef unsigned char           uint8_least;
typedef unsigned short          uint16_least;
typedef unsigned long           uint32_least;

typedef signed char             sint8_least;
typedef signed short            sint16_least;
typedef signed long             sint32_least;

typedef float                   float32;
typedef double                  float64;
/**
  * @}
  */


#endif /* PLATFORM_TYPES_H */
